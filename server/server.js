const express = require('express')

const cors = require('cors')

const bodyParser = require('body-parser')




const app = express()
app.use(cors('*'))
app.use(bodyParser.json())

// routers
const routerUser = require('./routes/user')
const routerAlbuum = require('./routes/album')
const routerArtist = require('./routes/artist')
const routerSong = require('./routes/song')

//static routing

app.use(express.static('./uploads'))


//add routers
app.use('/user', routerUser)
app.use('/album', routerAlbuum)
app.use('/artist', routerArtist)
app.use('/song', routerSong)




app.listen(4000,'0.0.0.0',()=> {
    console.log('server started on port number 4000')
})