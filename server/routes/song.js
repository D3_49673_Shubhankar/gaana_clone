const express =  require('express')
const router = express.Router()

const db = require('../db')
const util = require('../util')
const multer = require('multer')
const upload = multer({ dest: 'uploads/' })

router.get('/', (request, response) => {
    const query = `select 
      song.id, 
      song.title,
      song.duration,  
      song.thumbnail,  
      song.songfile,
      artist.firstName as artistFirstName, 
      artist.lastName as artistLastName,
      album.title as albumTitle
      from album, artist, song
      where song.albumId = album.id and album.artistId = artist.id`
    db.query(query, (error, artists) => {
      response.send(util.createResult(error, artists))
    })
  })

router.post('/', upload.single('thumbnail'), 
upload.single('songFile'),
  (request, response) => {
    const { title, artistId, albumId, duration } = request.body
  
    // get the uploaded file name
    const thumbnailFile = request.file.filename
    const songFile = request.file.filename
  
    const query = `insert into song (title, artistId, albumId, thumbnail, duration) values ('${title}', '${artistId}', '${albumId}', '${filename}', '${duration}')`
    db.query(query, (error, artists) => {
      response.send(util.createResult(error, artists))
    })
  })
  

module.exports = router