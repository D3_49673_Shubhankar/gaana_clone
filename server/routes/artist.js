const express =  require('express')
const router = express.Router()
const multer = require('multer')
const upload = multer({dest: 'uploads/'})


const db = require('../db')
const util = require('../util')


router.get('/', (request, response) => {
    const query = `select * from artist`
    db.query(query, (error, artists) => {
      response.send(util.createResult(error, artists))
    })
  })
  
  router.post('/', upload.single('thumbnail'), (request, response) => {
    const { firstName, lastName } = request.body

    const filename = request.file.filename
  
    const query = `insert into artist (firstName, lastName, thumbnail) values ('${firstName}', '${lastName}', '${filename}')`
    db.query(query, (error, artists) => {
      response.send(util.createResult(error, artists))
    })
  })


module.exports = router