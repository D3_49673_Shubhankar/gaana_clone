
const express =  require('express')

const router = express.Router()

const cryptoJS = require('crypto-js')
const db = require('../db')
const util = require('../util')


router.post('/signup', (request, response) =>{
    const {firstName, lastName, email, password} = request.body

    const encryptedPassword = cryptoJS.MD5(password)

    const query = `INSERT INTO 
                   USER (firstName, lastName, email, password)
                   VALUES('${firstName}','${lastName}','${email}','${encryptedPassword}')`
    db.query(query, (error,result) =>{
            response.send(util.createResult (error, result))
    })
})

router.post('/signin', (request, response) =>{
    const {email, password} = request.body

    const encryptedPassword = cryptoJS.MD5(password)

    const query = `SELECT * FROM USER WHERE email = '${email}' AND password = '${encryptedPassword}'`

    db.query(query, (error,result) =>{
        if(error){
            response.send(util.createError(error))
        } else{
            if(result.length == 0){
                response.send(util.createError('Invalid username or password'))
            } else response.send(util.createResult (error, result))
        }
    })
})

router.get('/profile/:id', (request, response) =>{
    const {id} = request.params

    
    const query = `SELECT * FROM USER WHERE id = ${id}`

    db.query(query, (error,result) =>{
        if(error){
            response.send(util.createError(error))
        } else{
            if(result.length == 0){
                response.send(util.createError('No user with given id '))
            } else response.send(util.createResult (error, result))
        }
    })
})

router.delete('/profile/:id', (request, response) =>{
    const {id} = request.params

    
    const query = `DELETE FROM USER WHERE id = ${id}`

    db.query(query, (error,result) =>{
        if(error){
            response.send(util.createError(error))
        } else{
            if(result.affectedRows == 0){
                response.send(util.createError('No user with given id '))
            } else response.send(util.createResult (error, result))
        }
    })
})


module.exports = router