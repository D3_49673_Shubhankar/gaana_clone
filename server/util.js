function createResult(error, data){
    const result = {}
        if(error){
            result['error'] = error
            result['status'] = 'error'
        } else{
            result['data'] = data
            result['status'] = 'success'
        }
    return result
}


function createError(error){
    const result = {}
        result['error'] = error
        result['status'] = 'error'
    return result
}
module.exports = {
    createResult: createResult,
    createError: createError
}