
import './App.css';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom'
import Home from './pages/Home';
import Discover from './pages/Discover';
import Browse from './pages/Browse';
import MyMusic from './pages/MyMusic';
import Signin from './pages/Signin';
import Signup from './pages/Signup';

function App() {
  return (
    <div >
     <BrowserRouter>

     <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <div className="container-fluid">

    <Link className="navbar-brand" to = "/home">Gana</Link> 
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
           <Link className="nav-link active" to = "/home">Home</Link> 
        </li>
        <li>
          <Link className="nav-link active" to = "/browse">Browse</Link>
        </li>
        <li>
          <Link className="nav-link active" to = "/discover">Discover</Link>
        </li>
        <li>      
          <Link className="nav-link active" to = "/mymusic">My Music</Link>
        </li>
        <li>      
          <Link className="nav-link active" to = "/signin">Signin</Link>
        </li>
      </ul>
      
    </div>
  </div>
</nav>
    
      
      <div className="container">
        
      
      
      <Switch>
        <Route path = "/home" component = {Home}/> 
        <Route path = "/discover" component = {Discover}/> 
        <Route path = "/browse" component = {Browse}/> 
        <Route path = "/mymusic" component = {MyMusic}/> 
        <Route path = "/signin" component = {Signin}/> 
        <Route path = "/signup" component = {Signup}/> 
      </Switch>

      </div>
     </BrowserRouter>
  
    </div>
  );
}

export default App;
