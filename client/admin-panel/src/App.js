import './App.css';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom'
import Artists from './pages/Artists';
import Albums from './pages/Albums';
import Users from './pages/Users';
import AddArtist from './pages/AddArtist';
import AddAlbum from './pages/AddAlbum';

function App() {
  return (
    <div >
     <BrowserRouter>

     <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <div className="container-fluid">

    <Link className="navbar-brand" to = "/home">Admin Portal</Link> 
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li className="nav-item">
           <Link className="nav-link active" to = "/artists">Artists</Link> 
        </li>
        <li>
          <Link className="nav-link active" to = "/albums">Albums</Link>
        </li>
        <li>
          <Link className="nav-link active" to = "/users">Users</Link>
        </li>
      </ul>
    </div>
  </div>
</nav>
    
      
      <div className="container">
        
      
      
      <Switch>
        <Route path = "/artists" component = {Artists}/> 
        <Route path = "/albums" component = {Albums}/> 
        <Route path = "/users" component = {Users}/>  
        <Route path = "/add-artist" component = {AddArtist}/>  
        <Route path = "/add-album" component = {AddAlbum}/>  
      </Switch>

      </div>
     </BrowserRouter>
  
    </div>
  );
}

export default App;
