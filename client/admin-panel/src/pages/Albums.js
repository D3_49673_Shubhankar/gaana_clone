import axios from "axios"
import { useState , useEffect} from "react"
import {url} from '../common/constants'
import AlbumRow from "../components/AulbmsRow"
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom'


const Albums = () =>{
    const [albums, setAlbum] = useState([])

    useEffect(() =>{
        console.log('album component got loaded')
        getAlbum()
    }, [])
    const getAlbum = ()=>{
        axios.get(url + '/album').then(response => {
            const result = response.data
            if(result.status === 'success'){
                setAlbum(result.data)
            } else {
                alert('error while loading list of artists')
            }
        })
    }
    return(
        <div>
            <h1 className = "page-title">Albums</h1>
            {/* <button onClick = {getArtist} className="btn btn-success">
                Get Artist
            </button> */}
            <Link to = '/add-album'> 
                <a  className="btn btn-success">
                    Add Album
                </a>

            </Link>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>id</th>
                        <th></th>
                        <th>Name</th>
                        <th>Artist</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    {albums.map((album) =>{
                        return (
                            <AlbumRow album = {album} />
                        )
                    })}
                </tbody>
            </table>
            
        </div>
    )
}

export default Albums