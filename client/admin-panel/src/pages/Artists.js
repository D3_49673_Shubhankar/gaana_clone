import axios from "axios"
import { useState , useEffect} from "react"
import {url} from '../common/constants'
import ArtistRow from "../components/ArtistsRow"
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom'

const Artists = () =>{
    const [artists, setArtists] = useState([])

    useEffect(() =>{
        console.log('artist component got loaded')
        getArtist()
    }, [])
    const getArtist = ()=>{
        axios.get(url + '/artist').then(response => {
            const result = response.data
            if(result.status === 'success'){
                setArtists(result.data)
            } else {
                alert('error while loading list of artists')
            }
        })
    }
    return(
        <div>
            <h1 className = "page-title">Artists</h1>
            <Link to = '/add-artist'> 
            <a  className="btn btn-success">
                Add Artist
            </a>

            </Link>
                     
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>id</th>
                        <th></th>
                        <th>Name</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    {artists.map((artist) =>{
                        return (
                            <ArtistRow artist = {artist}/ >
                        )
                    })}
                </tbody>
            </table>
            
        </div>
    )
}

export default Artists