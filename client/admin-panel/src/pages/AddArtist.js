import { Link , useHistory} from "react-router-dom"
import { useState } from 'react';
import axios from "axios";
import { url } from './../common/constants';

const AddArtist =() =>{
    const [thumbnail, setThumbnail] = useState(undefined)  
    const [firstName, setFirstName] = useState('')  
    const [lastName, setLastName] = useState('')  

    const history = useHistory()
    const onFileSelect = (event) =>{
        const file = event.target.files[0]
        setThumbnail(file)
    } 
    
    const addArtistToDb = ()=>{
        if(firstName.length === 0){
            alert('enter first name')
        } else if (lastName.length === 0){
            alert('enter last name')
        } else if (!thumbnail){
            alert('select image')
        } else {

            const body = new FormData()
            body.append('firstName', firstName)
            body.append('lastName', lastName)
            body.append('thumbnail', thumbnail)

            axios.post(url + '/artist', body).then((response) => {
                const result =  response.data
                if(result.status === 'success'){
                    alert('successfully added an artist')
                    history.push('/artists')
                } else {
                    alert('sonthing went wrong') }
            })
        }
    }
    return (
        <div>
            <h1 className = "page-title">Add Artist</h1>
            <div className = "mb-3">
                <label htmlFor = "">First Name</label>
                <input onChange = {(e) =>
                setFirstName(e.target.value)}  type = "text" className = "form-control"></input>
            </div>
            <div className = "mb-3">
                <label htmlFor = "">Last Name</label>
                <input 
                onChange = {(e) =>
                    setLastName(e.target.value)} 
                type = "text" className = "form-control"></input>
            </div>
            <div className = "mb-3">
                <label htmlFor = "">Thumbnail</label>
                <input accept = 'image/*' onChange = {onFileSelect}  type = "file" className = "form-control"></input>
            </div>

            <div className = "mb-3">
                <button onClick = {addArtistToDb} className="btn btn-success">Add</button>
                <Link to  = '/artists'>
                    <a className="btn btn-warning">Cancel</a>
                </Link>
            </div>
        </div>
    )
}

export default AddArtist