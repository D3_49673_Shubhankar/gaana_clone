import {url} from '../common/constants'
const AlbumRow = ({album})=>{
    return (
        <tr>
            <td>{album.id}</td>
            <td>
                <img src = {url + '/' + album.thumbnail} alt = "" className = "thumbnail-sm"/>
            </td>
            <td>{album.title}</td>
            <td>{album.artistFirstName} {album.artistLastName}</td>
            <td></td>
        </tr>
    )
}

export default AlbumRow